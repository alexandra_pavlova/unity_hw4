using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class SolveEquation : MonoBehaviour
{
    [SerializeField] private Text text;
    [SerializeField] private InputField inputFieldA;
    [SerializeField] private InputField inputFieldB;
    [SerializeField] private InputField inputFieldC;

    public void OnClickCompare()
    {
        double a, b, c, D, x1, x2;
        a = Convert.ToDouble(inputFieldA.text);
        b = Convert.ToDouble(inputFieldB.text);
        c = Convert.ToDouble(inputFieldC.text);
        
        if (a != 0)
        {
            D = b * b - 4 * a * c;
            if (D < 0)
            {
                text.text = "No solution";
            }
            else if (D == 0)
            {
                x1 = ((-1) * b + Math.Sqrt(D)) / (2 * a);
                text.text = x1.ToString();
                //x2 = ((-1) * b - Math.Sqrt(D)) / (2 * a);
            }
            else
            {
                x1 = ((-1) * b + Math.Sqrt(D)) / (2 * a);
                x2 = ((-1) * b - Math.Sqrt(D)) / (2 * a);
                text.text = x1.ToString() + ", " + x2.ToString();
            }
        }
        else
        {
            text.text = "Not a quadratic ecuation";
        }
    }
}

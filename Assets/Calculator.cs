using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class Calculator : MonoBehaviour
{
   [SerializeField] private Text text;
   [SerializeField] private InputField inputFieldA;
   [SerializeField] private InputField inputFieldB;

   public void OnClickSum()
   {
      text.text = ((Convert.ToInt32(inputFieldA.text))+(Convert.ToInt32(inputFieldB.text))).ToString();
   }
   
   public void OnClickDif()
   {
      text.text = ((Convert.ToInt32(inputFieldA.text))-(Convert.ToInt32(inputFieldB.text))).ToString();
   }
   
   public void OnClickMult()
   {
      text.text = ((Convert.ToInt32(inputFieldA.text))*(Convert.ToInt32(inputFieldB.text))).ToString();
   }
   
   public void OnClickDiv()
   {
      text.text = ((Convert.ToInt32(inputFieldA.text))/(Convert.ToInt32(inputFieldB.text))).ToString();
   }
}

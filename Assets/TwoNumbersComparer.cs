using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class TwoNumbersComparer : MonoBehaviour
{
    [SerializeField] private Text text;
    [SerializeField] private InputField inputFieldA;
    [SerializeField] private InputField inputFieldB;

    public void OnClickCompare()
    {
        if ((Convert.ToInt32(inputFieldA.text)) > (Convert.ToInt32(inputFieldB.text)))
        {
            text.text = inputFieldA.text;
        }
        else if ((Convert.ToInt32(inputFieldA.text)) < (Convert.ToInt32(inputFieldB.text)))
        {
            text.text = inputFieldB.text;
        }
        else
        {
            text.text = "Equal";
        }
    }
}

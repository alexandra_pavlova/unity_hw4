using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;


public class ThreeNumbersComparer : MonoBehaviour
{
	[SerializeField] private Text text;
    [SerializeField] private InputField inputFieldA;
    [SerializeField] private InputField inputFieldB;
	[SerializeField] private InputField inputFieldC;


    public void OnClickCompare()
	{
		int a, b, c;
		a = Convert.ToInt32(inputFieldA.text);
		b = Convert.ToInt32(inputFieldB.text);
		c = Convert.ToInt32(inputFieldC.text);

		if (a > b)
		{
			if (b > c)
			{
				text.text = inputFieldA.text + ", " + inputFieldB.text; // 1
			}
			else if (b < c)
			{
				if (c < a) 
				{
					text.text = inputFieldA.text + ", " + inputFieldC.text; // 2
				}
				else if (c > a)
				{
					text.text = inputFieldC.text + ", " + inputFieldA.text; // 5
				}
				else if ((a==c) && (a > b)) {text.text = inputFieldA.text + ", " + inputFieldC.text + " (Equal)";}
			}
			else if ((b==c) && (b < a)) {text.text = inputFieldA.text + " (Others are equal)";}
		}
		else if (b > a)
		{
			if (a > c)
			{
				text.text = inputFieldB.text + ", " + inputFieldA.text; // 3
			}
			else if (a < c) 
			{
				if (c < b)
				{
					text.text = inputFieldB.text + ", " + inputFieldC.text; // 4
				}
				else if (c > b)
				{
					text.text = inputFieldC.text + ", " + inputFieldB.text; // 6
				}
				else if ((b==c) && (b > a)) {text.text = inputFieldB.text + ", " + inputFieldC.text + " (Equal)";}
			}
			else if ((a==c) && (a < b)) {text.text = inputFieldB.text + " (Others are equal)";}
		}
		else if ((a==b) && (a > c)) {text.text = inputFieldA.text + ", " + inputFieldB.text + " (Equal)";}
		else if ((a==b) && (a < c)) {text.text = inputFieldC.text + " (Others are equal)";}
		else if ((a==b) && (a==c)) {text.text = "All are equal";}
	}


}
